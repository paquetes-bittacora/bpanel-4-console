# bPanel 4 console
Paquete (de prueba de momento) para automatizar algunas tareas comunes a la hora de desarrollar con bPanel 4. De momento solo mueve un paquete de bPanel 4 que se haya instalado por composer a la carpeta `packages` del proyecto, para poder editarlo.

Para usarlo, ejecutar lo siguiente en la consola

```bash
vendor/bin/bpanel4-console
```